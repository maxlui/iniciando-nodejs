const express = require("express");
const UserModel = require("../src/models/user.model");

const app = express();

app.get("/home", (req, res) => {
  res.contentType("application/html");
  res.status(200).send("<h1> Page home </h1>");
});

app.get("/users", (req, res) => {
  const users = [
    {
      name: "Maxlui",
      email: "maxlui@gmail.com",
    },
    {
      name: "Maria Paula",
      email: "mp@gmail.com",
    },
  ];
  res.status(200).json(users);
});

app.post("/users", async (req, res) => {
  try {
    const user = await UserModel.create(req.body);
    res.status(201).json(user);
  } catch (error) {
    req.status(500).send(error.message);
  }
});

const port = 8080;
app.listen(port, () => console.log(`Rodando com express na port ${port}!`));
