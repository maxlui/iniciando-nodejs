const fs = require("fs");
const path = require("path");

//criar uma pasta
// fs.mkdir(path.join(__dirname, "/teste"), {}, (error) => {
//   if (error) {
//     return console.log("Erro:", error);
//   }
//   console.log("Pasta criada com sucesso");
// });

//criando um arquivo
fs.writeFile(
  path.join(__dirname, "/teste", "teste.txt"),
  "HELLOW WORD - ",
  {},
  (error) => {
    if (error) {
      return console.log("Erro:", error);
    }
    console.log("Arquivo criado com sucesso");

    //ADICIONAR APPEND FILE
    fs.appendFile(
      path.join(__dirname, "/teste", "teste.txt"),
      "ola td bem?",
      (error) => {
        if (error) {
          return console.log("Erro:", error);
        }
        console.log("Arquivo Atualizado com sucesso");
      }
    );

    // ler arquivo
    fs.readFile(
      path.join(__dirname, "/teste", "teste.txt"),
      "utf8",
      (error, data) => {
        if (error) {
          return console.log("Erro:", error);
        }
        console.log(data);
      }
    );
  }
);
