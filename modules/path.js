const path = require("path");

// apenas o nome do arquivo
console.log(path.basename(__filename));

// nome do Diretorio atual
console.log(path.dirname(__filename));

//extensão do arquivo
console.log(path.extname(__filename));

//caminho completo com objeto
console.log(path.parse(__filename));

//juntar caminhos
console.log(path.join(__dirname, "teste", "teste.php"));
