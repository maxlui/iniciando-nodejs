class Person {
  constructor(name) {
    this.name = name;
  }

  digaMeuNome() {
    return `Meu nome é ${this.name}!`;
  }
}

module.exports = {
  Person,
};
