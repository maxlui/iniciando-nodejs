const mongoose = require("mongoose");

const connectToDatabase = async () => {
  await mongoose
    .connect(
      `mongodb+srv://${process.env.MONGODB_USERNAME}:${process.env.MONGODB_PASSWORD}@cluster0.8lhlw8t.mongodb.net/`
      // (error) => {
      //   if (error) {
      //     return console.log(
      //       "Ocorreu um erro ao tentar se conectar com o banco de dados",
      //       error
      //     );
      //   }
      //   return console.log("Conexão ao banco de dados realizada com sucesso");
      // }
    )
    .then(() => console.log("Conexão ao banco de dados realizada com sucesso"))

    .catch((err) => {
      console.error(
        "Ocorreu um erro ao tentar se conectar com o banco de dados ",
        err
      );
    });
};

module.exports = connectToDatabase;
